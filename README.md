# Virgil

> Virgil told the tales of Orpheus and Eurydice.

`virgil` is a package defining the Orphic response format used by Eris. It's
separated out to provide a lightweight library with no dependencies.

## Events

Virgil sends data as event streams, with each event having a type and a payload.
Payloads are well-defined for a given type.

The basic event structure is:

```json
{
  "type": "string",
  "payload": {}
}
```

### Result Event

Results from a query follow the `result` event type. The structure for one of
these is:

```json
{
  "type": "result",
  "payload": {
    "fields": [
      {
        "name": "string",
        "value": {}
      }
    ]
  }
}
```

### Error Event

Errors are returned as the following:

```json
{
  "type": "error",
  "payload": "error message"
}
```

It can be assumed the error message is safe for display to an end user.

### Control Event

Control events are used for things like marking the start and end of an event
stream.

```json
{
  "type": "control",
  "payload": "event"
}
```

## Original JSON Format

The original Virgil format is now deprecated.

Virgil JSON messages follow the format:

```json
{
  "data": [
    [
      "heading 1",
      "heading 2"
    ],
    [
      [
        "item 1",
        "item 2"
      ],
      [
        "item a",
        "item b"
      ]
    ]
  ],
  "messages": [
    {
      "type": "success",
      "text": "message",
      "fields": {
        "key": "value"
      }
    }
  ]
}
```

Both `data` and `messages` can be `null`, although typically there will always
be at least one entry in `messages`.

Each section of `data` can have 1 or more columns and 0 or more rows. Each row
will have the same number of columns as defined in the headings. The headings
are always strings, the data itself can be any value.

Each message in `messages` will contain a string `type` (one of `success`,
`failure`, `info`, `warn` or `error`), a string `text` message and 0 or more
`fields` with a string `key` and arbitrary typed `value`. 
