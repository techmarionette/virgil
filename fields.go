package virgil

import (
	"encoding/json"
	"math"
	"sort"
)

// Fields embellish a message. While fields accept interface{} they should only
// be used to store types that can be marshalled into JSON.
type Fields map[string]interface{}

// Keys returns a sorted slice of the keys for this set of fields.
func (f Fields) Keys() []string {
	var i int

	keys := make([]string, len(f))

	for k := range f {
		keys[i] = k
		i++
	}

	sort.Strings(keys)

	return keys
}

// MarshalJSON will marshal the fields, handling NaN and infinities.
func (f Fields) MarshalJSON() ([]byte, error) {
	fields := make(map[string]interface{}, len(f))

	for k, v := range f {
		if float, ok := v.(float64); ok {
			switch {
			case math.IsNaN(float):
				fields[k] = "NaN"
			case math.IsInf(float, 1):
				fields[k] = "+Inf"
			case math.IsInf(float, -1):
				fields[k] = "-Inf"
			default:
				fields[k] = float
			}
		} else {
			fields[k] = v
		}
	}

	//nolint:wrapcheck // Legacy implementation.
	return json.Marshal(fields)
}
