package virgil_test

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/techmarionette/virgil"
)

func ExampleMessage_String() {
	fmt.Println(virgil.Message{
		Type: virgil.SuccessMessage,
		Text: "Message body",
	})

	fmt.Println(virgil.Message{
		Type:   virgil.FailureMessage,
		Text:   "Message body",
		Fields: virgil.Fields{"error": "failure reason"},
	})

	fmt.Println(virgil.Message{
		Type:   virgil.InfoMessage,
		Text:   "Message body",
		Fields: virgil.Fields{"field": "value"},
	})

	fmt.Println(virgil.Message{
		Type: virgil.WarnMessage,
		Text: "Message body",
	})

	fmt.Println(virgil.Message{
		Type:   virgil.ErrorMessage,
		Text:   "Message body",
		Fields: virgil.Fields{"error": "failure reason"},
	})

	// Output:
	// [success] Message body
	// [failure] Message body {error:failure reason}
	// [info   ] Message body {field:value}
	// [warn   ] Message body
	// [error  ] Message body {error:failure reason}
}

func ExampleFailures() {
	fmt.Println(virgil.Failures([]virgil.Message{}))
	fmt.Println(virgil.Failures([]virgil.Message{
		{Type: virgil.SuccessMessage},
	}))
	fmt.Println(virgil.Failures([]virgil.Message{
		{Type: virgil.FailureMessage},
	}))
	fmt.Println(virgil.Failures([]virgil.Message{
		{Type: virgil.ErrorMessage},
	}))

	// Output:
	// false
	// false
	// true
	// true
}

func ExampleErrors() {
	fmt.Println(virgil.Errors([]virgil.Message{}))
	fmt.Println(virgil.Errors([]virgil.Message{
		{Type: virgil.SuccessMessage},
	}))
	fmt.Println(virgil.Errors([]virgil.Message{
		{Type: virgil.FailureMessage},
	}))
	fmt.Println(virgil.Errors([]virgil.Message{
		{Type: virgil.ErrorMessage},
	}))

	// Output:
	// false
	// false
	// false
	// true
}

func ExampleMessage() {
	m := virgil.Message{
		Type:   virgil.InfoMessage,
		Text:   "Message body",
		Fields: virgil.Fields{"field": "value"},
	}

	b, err := json.MarshalIndent(m, "", "  ")

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(b))

	// Output:
	// {
	//   "type": "info",
	//   "fields": {
	//     "field": "value"
	//   },
	//   "text": "Message body"
	// }
}
