package virgil

import "github.com/sirupsen/logrus"

// Logger is used to record log messages.
type Logger interface {

	// Success message, recorded when a particular operation has succeeded.
	// Success messages can exist with Failure and Error messages if an
	// operation was only partially successful.
	Success(msg string)

	// Failure message, recorded when a particular operation has failed. Failure
	// messages can exist with Success messages if an operation has only
	// partially failed. Failure messages are generally used to indicate that
	// one operation in a set of many has failed.
	Failure(msg string)

	// Info message using to indicate something has happened, or return data on
	// something that has happened.
	Info(msg string)

	// Warn message type, recorded when some unexpected event has occurred that
	// isn't classified as an error, or failure, but that may require
	// investigation.
	Warn(msg string)

	// Error message, recorded when a particular operation encounters an error.
	// Errors are typically internal application errors, which are unexpected.
	// Problems caused by incorrect input data should be reported as failures.
	Error(msg string)
}

type logger struct {
	fields   Fields
	filtered Fields
	channel  chan<- Message
}

const (
	typeField  = "message type"
	safeSuffix = " (reported)"
)

// NewLogger returns a new logger for the given fields. The logger reports
// messages to two destinations. Firstly a message with a filtered set of
// fields is put onto the given channel. Fields are filtered by removing error
// types and only reporting the safe message from Virgil error types. Secondly
// messages are logged via logrus using an embellished set of fields. Fields are
// embellished by the addition of the safe message from Virgil error types
// alongside the wrapped error message, plus the message type for failure and
// success messages. Messages will be logged at INFO level for Success and Info
// message types, WARN level for Failure and Warn message types, and ERROR level
// for Error message types.
func NewLogger(fields Fields, channel chan<- Message) Logger {
	filtered := make(Fields, len(fields))
	processed := make(Fields, len(fields))

	for k, v := range fields {
		switch v := v.(type) {
		case Error:
			filtered[k] = v.Safe()
			processed[k] = v.Error()
			processed[k+safeSuffix] = v.safe
		case error:
			processed[k] = v.Error()
		default:
			filtered[k] = v
			processed[k] = v
		}
	}

	return &logger{fields: processed, filtered: filtered, channel: channel}
}

func (l *logger) Success(msg string) {
	l.channel <- Message{Type: SuccessMessage,
		Text: msg, Fields: l.filtered}
	logrus.WithFields(l.embellish(SuccessMessage)).Info(msg)
}

func (l *logger) Failure(msg string) {
	l.channel <- Message{Type: FailureMessage,
		Text: msg, Fields: l.filtered}
	logrus.WithFields(l.embellish(FailureMessage)).Warn(msg)
}

func (l *logger) Info(msg string) {
	l.channel <- Message{Type: InfoMessage,
		Text: msg, Fields: l.filtered}
	logrus.WithFields(l.embellish(InfoMessage)).Info(msg)
}

func (l *logger) Warn(msg string) {
	l.channel <- Message{Type: WarnMessage,
		Text: msg, Fields: l.filtered}
	logrus.WithFields(l.embellish(WarnMessage)).Warn(msg)
}

func (l *logger) Error(msg string) {
	l.channel <- Message{Type: ErrorMessage,
		Text: msg, Fields: l.filtered}
	logrus.WithFields(l.embellish(ErrorMessage)).Error(msg)
}

func (l *logger) embellish(t Type) logrus.Fields {
	fields := logrus.Fields{}
	for k, v := range l.fields {
		fields[k] = v
	}

	if t == SuccessMessage || t == FailureMessage {
		fields[typeField] = t
	}

	return fields
}
