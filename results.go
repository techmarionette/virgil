package virgil

import (
	"fmt"
	"strings"
)

// Results returned from an Orphic query.
type Results interface {
	Columns() []string
	Rows() [][]interface{}
	String() string
}

// A Builder is used to build a set of Results.
type Builder interface {
	// Append a row to the builder. The row will either be padded with nils, or
	// truncated to match the columns set on the builder.
	Append(row ...interface{})

	// Overwrite the rows in the builder. The input rows are not checked for
	// validity.
	Overwrite(rows [][]interface{})

	// Results that have been built.
	Results() Results

	// Validate the rows in the builder.
	Validate() bool
}

type results struct {
	C []string        `json:"columns"`
	R [][]interface{} `json:"rows"`

	width int
}

// Empty returned if the result string is empty.
const Empty = "[empty]"

// NewBuilder returns a builder with the given columns.
func NewBuilder(columns ...string) Builder {
	return &results{C: columns, width: len(columns)}
}

func (r *results) Columns() []string {
	return r.C
}

func (r *results) Rows() [][]interface{} {
	return r.R
}

func (r *results) Append(row ...interface{}) {
	in := make([]interface{}, r.width)

	if len(row) > r.width {
		row = row[:r.width]
	}

	copy(in, row)

	r.R = append(r.R, in)
}

func (r *results) Overwrite(rows [][]interface{}) {
	r.R = rows
}

func (r *results) Results() Results {
	return r
}

func (r *results) Validate() bool {
	for _, row := range r.R {
		if len(row) != r.width {
			return false
		}
	}

	return true
}

func (r *results) String() string {
	if len(r.C) == 0 {
		return Empty
	}

	b := strings.Builder{}
	b.WriteString(strings.Join(r.Columns(), ", "))

	for _, row := range r.R {
		data := make([]string, len(row))

		for i, d := range row {
			data[i] = fmt.Sprintf("%v", d)
		}

		b.WriteString("\n")
		b.WriteString(strings.Join(data, ", "))
	}

	return b.String()
}

// EmptyResults returns a set of empty results.
func EmptyResults() Results {
	return &results{}
}

// SingleResult builds a Results type with a single item of data (i.e. one
// column, one row).
func SingleResult(name string, data interface{}) Results {
	return &results{
		C: []string{name},
		R: [][]interface{}{{data}},
	}
}
