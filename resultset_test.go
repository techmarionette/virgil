package virgil_test

import (
	"encoding/json"
	"testing"

	"bitbucket.org/techmarionette/virgil"
)

func TestResultSet_UnmarshalJSON(t *testing.T) {
	t.Run("a valid result set will unmarshal", func(t *testing.T) {
		t.Parallel()

		var target virgil.ResultSet

		source := virgil.ResultSet{
			Data: []virgil.Results{virgil.SingleResult("column", "data")},
			Messages: []virgil.Message{
				{
					Type:   virgil.SuccessMessage,
					Fields: virgil.Fields{"field": "data"},
					Text:   "message body",
				},
			},
		}

		if b, err := json.Marshal(source); err != nil {
			t.Errorf("unexpected error marshalling data: %s", err)
		} else if err := target.UnmarshalJSON(b); err != nil {
			t.Errorf("unexpected error unmarshalling data: %s", err)
		}

		switch {
		case len(target.Data) != 1:
			t.Errorf("target data set is wrong size: %d", len(target.Data))
		case len(target.Data[0].Columns()) != 1:
			t.Errorf("unexpected columns: %d", len(target.Data[0].Columns()))
		case target.Data[0].Columns()[0] != "column":
			t.Errorf("unexpected column name: %s", target.Data[0].Columns()[0])
		case len(target.Data[0].Rows()) != 1:
			t.Errorf("unexpected row stats: %d", len(target.Data[0].Rows()))
		case len(target.Data[0].Rows()[0]) != 1:
			t.Errorf("unexpected row width: %d", len(target.Data[0].Rows()[0]))
		case len(target.Messages) != 1:
			t.Errorf("unexpected message stats: %d", len(target.Messages))
		}
	})

	t.Run("Invalid input", func(t *testing.T) {
		t.Parallel()

		var target virgil.ResultSet

		if err := target.UnmarshalJSON([]byte{}); err == nil {
			t.Errorf("expected error unmarshalling data")
		}
	})

	t.Run("SHIRO-200", func(t *testing.T) {
		t.Parallel()

		var target virgil.ResultSet

		input := []byte(`
			{
			  "data": [
				{"columns": ["mappings"], "rows": [["org"]]},
				{
				  "columns": ["mappings"],
				  "rows": [
					["sanlocationmap"],
					["serverlocationmap"],
					["personlocationmap"],
					["locationmap"],
					["location"],
					["sans"],
					["servers"],
					["people"],
					["titles"]
				  ]
				}
			  ],
			  "messages": []
			}

		`)

		if err := target.UnmarshalJSON(input); err != nil {
			t.Errorf("expected error unmarshalling data: %v", err)
		}

		switch {
		case len(target.Data) != 2:
			t.Errorf("unexpected data length: %d", len(target.Data))
		case len(target.Data[0].Rows()) != 1:
			t.Errorf("unexpected row length: %d", len(target.Data[0].Rows()))
		case len(target.Data[1].Rows()) != 9:
			t.Errorf("unexpected row length: %d", len(target.Data[1].Rows()))
		}
	})
}
