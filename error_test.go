package virgil_test

import (
	"errors"
	"fmt"

	"bitbucket.org/techmarionette/virgil"
)

func ExampleNewError() {
	e := virgil.NewError("message")

	fmt.Println(e)
	fmt.Println(e.Safe())

	// Output:
	// message
	// message
}

func ExampleWrappedError() {
	e := virgil.WrappedError("safe", errors.New("unsafe"))

	fmt.Println(e)
	fmt.Println(e.Safe())

	// Output:
	// unsafe
	// safe
}
