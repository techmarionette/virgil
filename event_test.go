package virgil_test

import (
	"fmt"
	"testing"

	"bitbucket.org/techmarionette/virgil"
	"bitbucket.org/techmarionette/virgil/payloads"
	"github.com/stretchr/testify/assert"
)

func ExampleNewEvent() {
	payload := payloads.Result{Fields: []payloads.Field{
		{Key: "A", Value: 1}, {Key: "B", Value: 2},
	}}

	e := virgil.NewEvent(payload)

	fmt.Println(string(e.JSON()))

	// Output:
	// {"type":"result","payload":{"fields":[{"key":"A","value":1},{"key":"B","value":2}]}}
}

func ExampleUnmarshalEvent() {
	in := []byte(`{"type":"control","payload":"start"}`)

	e, err := virgil.UnmarshalEvent[payloads.Control](in)

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(e.Type)
		fmt.Println(e.Payload)
	}

	// Output:
	// control
	// start
}

func TestUnmarshalEvent(t *testing.T) {
	t.Run("An invalid event will error", func(t *testing.T) {
		t.Parallel()

		_, err := virgil.UnmarshalEvent[payloads.Control]([]byte{})

		assert.Error(t, err)
	})
}
