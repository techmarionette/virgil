module bitbucket.org/techmarionette/virgil

go 1.18

require (
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/testify v1.2.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20200413165638-669c56c373c4 // indirect
)
