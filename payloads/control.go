package payloads

// Control events are used for things like marking the start and end of an event
// stream, and reporting errors.
type Control string
