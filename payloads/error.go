package payloads

// Error event holding an error that is safe to display to an end user.
type Error string
