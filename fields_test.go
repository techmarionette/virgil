package virgil_test

import (
	"encoding/json"
	"fmt"
	"math"

	"bitbucket.org/techmarionette/virgil"
)

func ExampleFields_Keys() {
	fmt.Println(virgil.Fields{"a": "1", "b": "2"}.Keys())
	fmt.Println(virgil.Fields{"b": "1", "a": "2"}.Keys())

	// Output:
	// [a b]
	// [a b]
}

func ExampleFields_MarshalJSON() {
	b, err := json.Marshal(virgil.Fields{
		"a": math.NaN(),
		"b": math.Inf(1),
		"c": math.Inf(-1),
		"d": 0.0,
	})

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(b))

	// Output:
	// {"a":"NaN","b":"+Inf","c":"-Inf","d":0}
}
