package virgil

import (
	"fmt"
	"sort"
	"strings"
)

// Type of Message being sent. Many message of the same type can appear in a set
// of message as an action may involve many operations.
type Type string

// Message is used to pass log/event style messages to the client.
type Message struct {
	Type   Type   `json:"type"`
	Fields Fields `json:"fields"`

	// Text payload of the message.
	Text string `json:"text"`
}

const (
	// SuccessMessage is generated for an operation that succeeded.
	SuccessMessage = Type("success")

	// FailureMessage is generated for an operation that failed due to invalid
	// input (as opposed to server error).
	FailureMessage = Type("failure")

	// InfoMessage is an informational message about an operation.
	InfoMessage = Type("info")

	// WarnMessage is a warning about an operation. A warning is not as severe
	// as a failure or an error, but may indicate that something is not right.
	WarnMessage = Type("warn")

	// ErrorMessage is generated if the server encountered an error while
	// processing an action. The error will have stopped some, or all of the
	// operation from completing.
	ErrorMessage = Type("error")
)

// String representation of a message. The message fields are flattened into the
// string.
func (m Message) String() string {
	var (
		i    int
		data string
	)

	fields := make([]string, len(m.Fields))

	for k, v := range m.Fields {
		fields[i] = fmt.Sprintf("%s:%v", k, v)
		i++
	}

	if len(fields) > 0 {
		sort.Strings(fields)
		data = fmt.Sprintf(" {%s}", strings.Join(fields, ", "))
	}

	return fmt.Sprintf("[%-7s] %s%s", m.Type, m.Text, data)
}

// Failures returns true if any of the messages provided are failure or error
// messages.
func Failures(msgs []Message) bool {
	for _, m := range msgs {
		if m.Type == ErrorMessage || m.Type == FailureMessage {
			return true
		}
	}

	return false
}

// Errors returns true if any of the messages provided are error messages.
func Errors(msgs []Message) bool {
	for _, m := range msgs {
		if m.Type == ErrorMessage {
			return true
		}
	}

	return false
}
