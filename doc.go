// Package virgil defines the communication protocol between Eris and the
// outside world.
//
// (c)2020 Tech Marionette Limited.
package virgil
