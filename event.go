package virgil

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/techmarionette/virgil/payloads"
)

// Payload of an event.
type Payload interface {
	payloads.Control | payloads.Error | payloads.Result
}

// Event with a specific Type and Payload. Type is dependent on Payload.
type Event[P Payload] struct {
	Type    string `json:"type"`
	Payload P      `json:"payload"`
}

// NewEvent returns an Event for the given Payload.
func NewEvent[P Payload](payload P) *Event[P] {
	return &Event[P]{
		Type:    strings.ToLower(reflect.TypeOf(payload).Name()),
		Payload: payload,
	}
}

// UnmarshalEvent will unmarshal JSON into an Event.
func UnmarshalEvent[P Payload](b []byte) (*Event[P], error) {
	var result Event[P]

	err := json.Unmarshal(b, &result)

	if err != nil {
		err = fmt.Errorf("failed to unmarshal event: %w", err)
	}

	return &result, err
}

// JSON representation of the event.
func (e *Event[P]) JSON() []byte {
	//nolint:errchkjson // Payload types will always be marshallable.
	b, _ := json.Marshal(e)

	return b
}
