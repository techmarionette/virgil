package virgil

// Error type that can be used to record and log and error while not potentially
// exposing implementation details or data. Virgil will attempt to discard all
// standard error types, although will log the safe messages from a Virgil
// error type.
type Error struct {
	safe   string
	unsafe string
}

// NewError returns a new Virgil error type with a message which will be used
// for both the safe and unsafe messages.
func NewError(msg string) Error {
	return Error{safe: msg, unsafe: msg}
}

// WrappedError wraps an existing error providing a safe client facing message
// to use.
func WrappedError(msg string, err error) Error {
	return Error{safe: msg, unsafe: err.Error()}
}

// Error returns the unsafe error message. Since this is replicating an error
// type we need to assume that if it's cast to error we may be leaking the
// message.
func (e Error) Error() string {
	return e.unsafe
}

// Safe error message recorded by this error type.
func (e Error) Safe() string {
	return e.safe
}
