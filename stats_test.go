package virgil_test

import (
	"fmt"

	"bitbucket.org/techmarionette/virgil"
)

func ExampleNewStats() {
	m := []virgil.Message{
		{Type: virgil.SuccessMessage},
		{Type: virgil.FailureMessage},
		{Type: virgil.InfoMessage},
		{Type: virgil.WarnMessage},
		{Type: virgil.ErrorMessage},
	}

	fmt.Println(virgil.NewStats(m))

	// Output:
	// {1 1 1 1 1}
}

func ExampleStats_Errors() {
	c := virgil.Stats{}

	if !c.Errors() {
		c.Failure++
	}

	if !c.Errors() {
		c.Error++
	}

	if c.Errors() {
		fmt.Println(c)
	}

	// Output:
	// {0 1 0 0 1}
}

func ExampleStats_Failures() {
	c := virgil.Stats{}

	if !c.Failures() {
		c.Error++
	}

	if c.Failures() {
		c.Failure++
	}

	if c.Failures() {
		fmt.Println(c)
	}

	// Output:
	// {0 1 0 0 1}
}

func ExampleStats_Delta() {
	l := virgil.NewLog()
	l.Failure("a failure")

	c := l.Checkpoint()

	l.Error("an error")

	d := l.Checkpoint().Delta(c)

	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(l.Checkpoint())

	// Output:
	// {0 1 0 0 0}
	// {0 0 0 0 1}
	// {0 1 0 0 1}
}
