package virgil_test

import (
	"errors"
	"fmt"

	"bitbucket.org/techmarionette/virgil"
)

func ExampleNewLog() {
	l := virgil.NewLog()

	l.WithField("test", "field").Success("A success message")
	l.WithFields(virgil.Fields{"a": "1", "b": "2"}).Info("An info message")
	l.Failure("A failure message")
	l.Warn("A warn message")
	l.WithField("error", errors.New("e")).Error("An error message")
	l.WithField("safe", virgil.NewError("error")).Error("A safe error")

	for _, message := range l.Events() {
		fmt.Println(message.String())
	}

	// Output:
	// [success] A success message {test:field}
	// [info   ] An info message {a:1, b:2}
	// [failure] A failure message
	// [warn   ] A warn message
	// [error  ] An error message
	// [error  ] A safe error {safe:error}
}
