package virgil

import "encoding/json"

// ResultSet created by Eris when some Orphic has been run. Each element in Data
// can be a different width.
type ResultSet struct {
	Data []Results `json:"data"`

	// Messages generated while running the Orphic.
	Messages []Message `json:"messages"`
}

// UnmarshalJSON handles unmarshalling a JSON representation of the ResultSet.
func (r *ResultSet) UnmarshalJSON(b []byte) error {
	var target struct {
		Data     []results
		Messages []Message
	}

	if err := json.Unmarshal(b, &target); err != nil {
		//nolint:wrapcheck // Legacy implementation.
		return err
	}

	r.Data = make([]Results, len(target.Data))

	for i, d := range target.Data {
		b := NewBuilder(d.C...)
		b.Overwrite(d.R)
		r.Data[i] = b.Results()
	}

	r.Messages = target.Messages

	return nil
}
