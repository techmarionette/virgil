package payloads

// Field in a set of results.
type Field struct {
	Key   string `json:"key"`
	Value any    `json:"value"`
}

// Result event containing an ordered set of fields.
type Result struct {
	Fields []Field `json:"fields"`
}
