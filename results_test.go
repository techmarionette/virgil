package virgil_test

import (
	"fmt"

	"bitbucket.org/techmarionette/virgil"
)

func ExampleNewBuilder() {
	b := virgil.NewBuilder("column 1", "column 2")
	b.Append("a", "b")
	b.Append("A", "B", "C")
	b.Append("1")

	fmt.Println(b.Results())

	b.Overwrite([][]interface{}{{"1", "2"}})

	fmt.Println(b.Results())

	// Output:
	// column 1, column 2
	// a, b
	// A, B
	// 1, <nil>
	// column 1, column 2
	// 1, 2
}

func ExampleBuilder_Validate() {
	b := virgil.NewBuilder("column 1", "column 2")

	b.Overwrite([][]interface{}{{"data"}})
	fmt.Println(b.Validate())

	b.Overwrite([][]interface{}{{"1", "2"}})
	fmt.Println(b.Validate())

	// Output:
	// false
	// true
}

func ExampleEmptyResults() {
	fmt.Println(virgil.EmptyResults())

	// Output:
	// [empty]
}

func ExampleSingleResult() {
	r := virgil.SingleResult("column", "data")
	fmt.Println(r.Columns())
	fmt.Println(r.Rows())

	// Output:
	// [column]
	// [[data]]
}
