package virgil

// Stats block for message types.
type Stats struct {
	Success int
	Failure int
	Info    int
	Warn    int
	Error   int
}

// NewStats returns a set of stats for the given messages. Unknown message tpyes
// are treated as errors.
func NewStats(messages []Message) Stats {
	stats := Stats{}

	for _, message := range messages {
		switch message.Type {
		case SuccessMessage:
			stats.Success++
		case FailureMessage:
			stats.Failure++
		case InfoMessage:
			stats.Info++
		case WarnMessage:
			stats.Warn++
		case ErrorMessage:
			stats.Error++
		case marker:
		}
	}

	return stats
}

// Failures returns true if the stats contain any Failure or Error message
// types.
func (s Stats) Failures() bool {
	return s.Failure > 0 || s.Error > 0
}

// Errors returns true if the stats contain any Error message types.
func (s Stats) Errors() bool {
	return s.Error > 0
}

// Delta returns the different between two sets of stats, subtracting the
// submitted stats from the current stats.
func (s Stats) Delta(stats Stats) Stats {
	return Stats{
		Success: s.Success - stats.Success,
		Failure: s.Failure - stats.Failure,
		Info:    s.Info - stats.Info,
		Warn:    s.Warn - stats.Warn,
		Error:   s.Error - stats.Error,
	}
}
